# chess

#### 介绍

一个使用 springboot 3.2.4 和 java21 版本实现的国际象棋 web 小程序

#### 软件架构

没啥软件架构，就是使用了暴力算法+剪枝，然后仿照了 lichess 和 chesscom 的一些引擎设置，比如计算深度，前n种最佳着法，叶子节点深度以及使用多少线程数计算

#### 使用说明

启动ChessApplication，在浏览器中输入http://localhost:8080/

界面如下

![初始界面](https://foruda.gitee.com/images/1711730121350659285/daa10bfe_43183.png "屏幕截图")

在 fen 输入框输入国象初始局面或中局，残局局面，有关 fen 格式文件概念，一般国象爱好者都知道，如果是程序员同行请自行搜索相关介绍

在 depth 输入计算深度

在 width 输入前n种最佳着法

在 pool_size 输入想使用多少线程数来计算

在 leaf_depth 输入需要计算的叶子节点深度

按 submit 按钮开始游戏

开始局面如下图

![对局图](https://foruda.gitee.com/images/1711730314774069587/c69cd118_43183.png "屏幕截图")

对局中各个参数可以随时调整，目前做的还比较简陋，只能让电脑自己和自己走棋，通过点击 submit 按钮看对局情况

界面下方会显示实时的谱着，但是只显示上一步，还需要优化和扩展

### 单元测试

运行com.wujunshen.chess.TestChess代码，其中有 3 个测试方法

一个是测试残局黑双车杀单王

一个是测试某个中局局面

最后一个是测试开局局面

见下图

![3 个测试方法](lib/img.png)

每个方法后面是单个测试方法运行时间

测试残局黑双车杀单王打印信息如下


```
.......k
........
........
........
........
........
rr......
......K.
Side: BLACK
batch_run depth=1
cut_children  cut_depth=0 cut_width=23
0 - 23
1 - 0
ROOK a2a1 10114.8
ROOK b2b1 10114.3
ROOK b2g2 115.800156
。。。当中略过。。。
ROOK b2b8 108.90023
best move is (ROOK,a2a1,10114.8)

```

测试某个中局局面打印信息如下

```
.rbr..k.
.pppnpp.
p.n.p..p
....Pq..
..PP....
PQ..BN.P
..P.BPP.
R....RK.
Side: BLACK
batch_run depth=1
cut_children  cut_depth=0 cut_width=23
0 - 23
1 - 0
ROOK d8f8 -4.8001223
KING g8h7 -4.9000525
PAWN b7b6 -5.00022
。。。当中略过。。。
QUEEN f5g4 -14.20022
KNIGHT c6a7 -18.200024
best move is (ROOK,d8f8,-4.8001223)
```

测试开局局面打印信息如下

```
rnbqkbnr
pppppppp
........
........
........
........
PPPPPPPP
RNBQKBNR
Side: WHITE
batch_run depth=1
cut_children  cut_depth=0 cut_width=23
0 - 20
1 - 0
PAWN e2e4 9.099609
PAWN d2d4 8.799707
KNIGHT g1f3 8.199804
KNIGHT b1c3 8.199804
PAWN e2e3 7.399658
PAWN d2d3 6.399756
KNIGHT g1h3 3.3999023
KNIGHT b1a3 3.3999023
PAWN h2h4 2.0999024
。。。当中略过。。。
PAWN c2c3 0.7998535
PAWN f2f3 0.19990234
best move is (PAWN,e2e4,9.099609)
```

### 写在最后

随便玩玩，主要给 java 程序员演示国际象棋用的，目前的计算由于使用的是暴力算法，因此比较 low，后续会改进算法

