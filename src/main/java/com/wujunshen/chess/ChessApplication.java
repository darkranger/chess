package com.wujunshen.chess;

import com.github.bhlangonijr.chesslib.move.Move;
import com.wujunshen.chess.common.ChessProperties;
import com.wujunshen.chess.engine.Node;
import com.wujunshen.chess.engine.ThreadEngine;
import com.wujunshen.chess.engine.TreeEngine;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wujunshen
 */
@SpringBootApplication
@RestController
public class ChessApplication {

  public static void main(String[] args) {
    SpringApplication.run(ChessApplication.class, args);
  }

  @GetMapping("get")
  public Move get(
      String fen,
      int depth,
      int width,
      @RequestParam("pool_size") int poolSize,
      @RequestParam("leaf_depth") int leafDepth) {
    ChessProperties chessProperties = new ChessProperties();
    chessProperties.setDepth(depth);
    chessProperties.setWidth(width);
    chessProperties.setPoolSize(poolSize);
    chessProperties.setLeafDepth(leafDepth);

    StartPosition startPosition = new StartPosition();
    startPosition.load(fen);

    Node rootNode = new Node(startPosition);
    rootNode.evaluate();

    TreeEngine treeEngine = new TreeEngine();
    treeEngine.buildTree(rootNode, 0, depth);

    return ThreadEngine.batchRun(rootNode, chessProperties);
  }
}
