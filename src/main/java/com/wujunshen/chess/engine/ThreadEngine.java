package com.wujunshen.chess.engine;

import com.github.bhlangonijr.chesslib.PieceType;
import com.github.bhlangonijr.chesslib.move.Move;
import com.wujunshen.chess.common.ChessProperties;
import java.util.concurrent.*;
import org.apache.commons.lang3.tuple.Triple;

/**
 * @author wujunshen
 */
public class ThreadEngine {
  public static Move batchRun(Node node, ChessProperties chessProperties) {
    try (ExecutorService executor = Executors.newFixedThreadPool(chessProperties.getPoolSize())) {
      // 创建一个Callable任务
      Callable<Triple<PieceType, Move, Float>> task =
          () -> {
            SearchEngine searchEngine = new SearchEngine();
            return searchEngine.search(node, chessProperties);
          };

      // 提交任务并获取Future对象
      Future<Triple<PieceType, Move, Float>> future = executor.submit(task);

      try {
        // 等待任务执行完成并获取结果
        Triple<PieceType, Move, Float> bestMove = future.get();
        System.out.println("best move is " + bestMove);

        return bestMove.getMiddle();
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }
  }
}
