package com.wujunshen.chess.common;

import lombok.Data;

/**
 * @author frank woo(吴峻申) <br>
 *     email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2024/3/29 23:49<br>
 */
@Data
public class ChessProperties {
  // 根据当前局面计算未来多少步，也就是树的层级数
  private int depth;
  private int width;
  private int poolSize;
  private int leafDepth;
}
