package com.wujunshen.chess.common;

/**
 * @author frank woo(吴峻申) <br>
 *     email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2024/3/29 21:33<br>
 */
public class Constants {
  public static final String LOSS = "loss";
  public static final String WIN = "win";
  public static final String DRAW = "draw";
  public static final int CUT_WIDTH = 23;
}
