package com.wujunshen.chess;

import com.wujunshen.chess.common.ChessProperties;
import com.wujunshen.chess.engine.Node;
import com.wujunshen.chess.engine.ThreadEngine;
import com.wujunshen.chess.engine.TreeEngine;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestChess {
  private ChessProperties chessProperties = new ChessProperties();

  @Before
  public void init() {
    chessProperties.setDepth(4);
    chessProperties.setWidth(16);
    chessProperties.setPoolSize(4096);
    chessProperties.setLeafDepth(1);
  }

  @After
  public void destroy() {
    chessProperties = null;
  }

  @Test
  public void testOpenGame() {
    StartPosition startPosition = new StartPosition();
    startPosition.load("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");

    Node rootNode = new Node(startPosition);
    rootNode.evaluate();
    System.out.println("root node is \n" + rootNode);

    TreeEngine treeEngine = new TreeEngine();
    treeEngine.buildTree(rootNode, 0, chessProperties.getDepth());
    System.out.println("all the nodes of tree is \n" + rootNode);

    ThreadEngine.batchRun(rootNode, chessProperties);
  }

  @Test
  public void testMiddleGame() {
    StartPosition startPosition = new StartPosition();
    startPosition.load("1rbr2k1/1pppnpp1/p1n1p2p/4Pq2/2PP4/PQ2BN1P/2P1BPP1/R4RK1 b - - 1 14");

    Node rootNode = new Node(startPosition);
    rootNode.evaluate();
    System.out.println("root node is \n" + rootNode);

    TreeEngine treeEngine = new TreeEngine();
    treeEngine.buildTree(rootNode, 0, chessProperties.getDepth());
    System.out.println("all the nodes of tree is \n" + rootNode);

    ThreadEngine.batchRun(rootNode, chessProperties);
  }

  @Test
  public void testEndGame() {
    StartPosition startPosition = new StartPosition();
    startPosition.load("7k/8/8/8/8/8/rr6/6K1 b - - 4 79");

    Node rootNode = new Node(startPosition);
    rootNode.evaluate();
    System.out.println("root node is \n" + rootNode);

    TreeEngine treeEngine = new TreeEngine();
    treeEngine.buildTree(rootNode, 0, chessProperties.getDepth());
    System.out.println("all the nodes of tree is \n" + rootNode);

    ThreadEngine.batchRun(rootNode, chessProperties);
  }
}
